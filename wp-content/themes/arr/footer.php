<footer class="footer" id="footer">
  <div class="logo">
    <a data-scroll href="#header">
      <?php
      if (has_custom_logo())  :
        $custom_logo_id = get_theme_mod( 'custom_logo' );
        $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
        ?>
        <img class="footer__logo" src="<?php echo $logo[0];?>" alt="">

      <?php else :
        ?>
        <img class="footer__logo" src="<?php
        bloginfo( 'template_url' ); ?>/assets/img/logo.png" alt="">
      <?php
      endif;
      ?>
    </a>
  </div>
  <div class="footer__wrapper">
    <div class="footer__social">
      <div class="footer__social__wrapper">
        <div class="footer__social__items">
          <a href="https://www.behance.net/agatareclaf" target="_blank" rel="noopener noreferrer">
            <img src="<?php
            bloginfo( 'template_url' ); ?>/assets/img/behance.png" alt="">
          </a>
          <a href="https://www.facebook.com/agata.reclaf.3" target="_blank" rel="noopener noreferrer">
            <img src="<?php
            bloginfo( 'template_url' ); ?>/assets/img/facebook.png" alt="">
          </a>
          <a href="https://www.pinterest.it/agatareclafphotography/" target="_blank" rel="noopener noreferrer">
            <img src="<?php
            bloginfo( 'template_url' ); ?>/assets/img/pinterest.png" alt="">
          </a>
          <a href="https://www.instagram.com/reclaf_photography/" target="_blank" rel="noopener noreferrer">
            <img src="<?php
            bloginfo( 'template_url' ); ?>/assets/img/instagram.png" alt="">
          </a>
        </div>
        <p class="footer__social__rights">All rights reserved</p>
        <p class="footer__social__rights mt-10 ">rebuilt by: Kamil.</p>
      </div>
    </div>
    <div class="footer__contact">
      <div class="footer__contact__wrapper">
        <p class="footer__contact__title">Let's stay in touch:</p>
        <a class="footer__contact__mail" href="mailto:contact@agatareclaf.com">contact@agatareclaf.com</a>
      </div>
    </div>
  </div>
</footer>
<?php wp_footer();?>
</body>

</html>