<!DOCTYPE html>
<html <?php
language_attributes(); ?>>
<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?php
  bloginfo( 'description' ); ?>">
  <link rel="canonical" href="<?php
  echo site_url(); ?>"/>
  <meta property="og:locale" content="en_GB"/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="Home &middot; AGATA RECLAF PHOTOGRAPHY"/>
  <meta property="og:url" content="<?php
  echo site_url(); ?>"/>
  <meta property="og:site_name" content="AGATA RECLAF PHOTOGRAPHY"/>
  <meta property="og:image"
        content="<?php
        bloginfo( 'template_url' ); ?>/assets/img/IMG_2210_Agata_Reclaf_photography_napis.jpg"/>
  <meta property="og:image:width" content="3000"/>
  <meta property="og:image:height" content="1800"/>
  <meta name="twitter:card" content="summary"/>
  <meta name="twitter:title" content="Home &middot; AGATA RECLAF PHOTOGRAPHY"/>
  <meta name="twitter:image"
        content="<?php
        bloginfo( 'template_url' ); ?>/assets/img/IMG_2210_Agata_Reclaf_photography_napis.jpg"/>
  <link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,600,700" rel="stylesheet">
  <?php
  wp_head();
  ?>
  <!-- <link rel="stylesheet" href="<?php
  bloginfo( 'template_url' ); ?>/assets/tiny-slider/dist/tiny-slider.css"> -->
</head>

<body <?php
body_class();
?>>
<header class="header" id="header">
  <div class="header__wrapper">
    <div class="header__container">
      <a class="header__title" href="<?php
      echo site_url() ?>">
        <?php
        if (has_custom_logo())  :
          $custom_logo_id = get_theme_mod( 'custom_logo' );
          $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
          ?>
          <img class="header__title__logo" src="<?php echo $logo[0];?>" alt="">

        <?php else :
          ?>
          <img class="header__title__logo" src="<?php
          bloginfo( 'template_url' ); ?>/assets/img/logo.png" alt="">
        <?php
        endif;
        ?>
        <h1 class="header__title__name">RECLAF</h1>
      </a>
      <nav>
        <button class="hamburger" aria-expanded="false" id="js-menu-button">
          <span class="sr-only">Otwórz/zamknij menu</span>
          <span class="hamburger__box"><span class="hamburger__inner"></span></span>
        </button>
        <div class="navigation">
          <ul class="navigation__list">
            <li class="navigation__item">
              <a href="<?php
              echo site_url() ?>" class="menu__item__link">home</a>
            </li>
            <li class="navigation__item">
              <a data-scroll href="<?php
              echo site_url() ?>/#projects" class="menu__item__link">projects</a>
            </li>
            <li class="navigation__item">
              <a data-scroll href="<?php
              echo site_url() ?>/#about" class="menu__item__link">about</a>
            </li>
            <li class="navigation__item">
              <a data-scroll href="<?php
              echo site_url() ?>/#footer" class="menu__item__link">contact</a>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  </div>
</header>