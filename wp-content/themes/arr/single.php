<?php
get_header(); ?>
  single view
  <div class="wrapper--single">
    <h2><?php
      the_title()
      ?></h2>
    <?php
    if ( have_posts() ) : while ( have_posts() ) : the_post();

      the_content();
    endwhile;
    endif;
    ?>
  </div>
<?php
get_footer(); ?>