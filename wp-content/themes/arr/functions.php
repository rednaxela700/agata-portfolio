<?php
//let wordpress manage site title
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );

//stylesheet hook
function load_stylesheets() {
  wp_register_style( 'style', get_template_directory_uri() . '/style.css', [], true );
  wp_enqueue_style('style');
}
//custom logo
function theme_prefix_setup() {

  add_theme_support( 'custom-logo', array(
    'height'      => 40,
    'width'       => 40,
    'flex-width' => true,
  ) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );
//js hook
function load_js(){
  $script_dir = 'https://cdn.jsdelivr.net/gh/cferdinandi/smooth-scroll@14.0.0/dist/smooth-scroll.polyfills.min.js';
  wp_register_script('smoothScroll', $script_dir, [], false, true);
  wp_register_script('customSmoothScroll', get_template_directory_uri() . '/js/scroll.js', ['smoothScroll'], false, true);
  wp_enqueue_script('customSmoothScroll');
  wp_register_script('menu', get_template_directory_uri() . '/js/menu.js', [], false, true);
  wp_enqueue_script('menu');
}

load_stylesheets();
load_js();