<?php
get_header(); ?>
<div class="wrapper--page">
  <main class="container--page">
    <h2><?php
      the_title()
      ?></h2>
  <?php
  if ( have_posts() ) : while ( have_posts() ) : the_post();

    the_content();
  endwhile;
  endif;
  ?>
  </main>
</div>
<?php
get_footer(); ?>

