<?php
get_header(); ?>
  <section class="hero">
    <div class="hero__wrapper">
      <div class="hero__container">
        <h2 class="hero__title">NOTHING</h2>
        <p class="hero__subtitle">is more revealing than nudity, even when it’s dressed.</p>
        <div class="hero__container__divider"></div>
        <p class="hero__container__desc hero__container__desc--small">HELMUT NEWTON</p>
      </div>
    </div>
  </section>
  <main id="projects">
    <div class="main">
      <?php
      get_template_part( 'template-parts/content', 'projects' );
      get_template_part( 'template-parts/content', 'about' );
      ?>
    </div>
  </main>
<?php
get_footer();