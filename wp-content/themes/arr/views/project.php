<link rel="stylesheet" href="<?php
bloginfo( 'template_url' ); ?>/src/project.css">
<link href="<?php
bloginfo( 'template_url' ); ?>/assets/glightbox-master/dist/css/glightbox.css" rel="stylesheet">
<script src="<?php
bloginfo( 'template_url' ); ?>/assets/glightbox-master/dist/js/glightbox.js"></script>
<?php

$ID = get_the_ID();

$all = array(
  "limit" => - 1,
);

$projects = pods( 'projects', $all );

$counter = 0;

if ( 0 < $projects->total() ) :
  while ( $projects->fetch( $ID ) and $counter == 0 ) :?>
    <?php
    $arr_gallery = explode( ' ', $projects->display( 'galeria' ) );
    ?>
    <div class="container small" style="margin-top: 60px;">
      <main class="main">
        <section class="project">
          <div class="project__description">
            <div class="project__description__title"><?php
              echo $projects->display( 'nazwa_projektu' ) ?></div>
            <div class="project__description__text">
              <p class="project__description__content">
              <p><?php
                echo get_post_field( 'post_content', $ID ) ?></p>
            </div>
          </div>
        </section>
        <div class="thumbnails thumbnails--both">
          <div class="thumbnails__slider">
            <?php
            for ( $x = 0; $x < count( $arr_gallery ); $x ++ ) :
              if ( $x % 3 === 0 ) : ?>
                <div class=" big_pic">
                  <img src="<?php
                  echo $arr_gallery[ $x ] ?>" alt="image" class="thumbnails__slider__photo">
                </div>
              <?php
              else : ?>
                <div class="med_pic">
                  <img src="<?php
                  echo $arr_gallery[ $x ] ?>" alt="image" class="thumbnails__slider__photo">
                </div>
              <?php
              endif; ?>
            <?php
            endfor; ?>
          </div>
        </div>
      </main>
    </div>
    <?php
    $counter ++;
  endwhile;
endif;

?>

<script>
  var myLightbox = GLightbox({
    selector: 'glightbox-demo',
    closeButton: true,

  });
</script>


  