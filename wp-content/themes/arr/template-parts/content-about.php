<section class="about">
  <h2 class="about__title" id="about">ABOUT</h2>
  <div class="about__content">
    <div class="about__content__left">
      <img class="about__content__left__portrait" src="<?php
      echo site_url() ?>/wp-content/themes/arr/assets/img/agatka.jpg" alt="">
    </div>
    <div class="about__content__right">
      <p class="about__content__right__title">I’m a photographer. Based in Gdańsk – Poland.</p>
      <div class="about__section">
        <p class="about__section__title">Solo exhibitions:</p>
        <ul class="about__section__list">

          <?php
          $params = array(
            'limit'   => - 1,
            'orderby' => 'date DESC'
          );
          $solo   = pods( 'solo_exhibitions', $params );

          if ( 0 < $solo->total() ) {
            while ( $solo->fetch() ) {
              ?>
              <li class="about__section__list__item">
                <span class="about__section__list__item__date"><?php
                  echo $solo->display( 'event_date' ) ?></span>
                <span class="about__section__list__item__desc"><?php
                  echo $solo->display( 'description' )
                  ?></span>
              </li>
              <?php
            }
          }
          ?>
        </ul>
      </div>
      <div class="about__section">
        <p class="about__section__title">Group exhibitions:</p>
        <ul class="about__section__list">
          <?php
          $groups = pods( 'group_exhibitions', $params );

          if ( 0 < $groups->total() ) {
            while ( $groups->fetch() ) {
              ?>
              <li class="about__section__list__item">
                <span class="about__section__list__item__date"><?php
                  echo $groups->display( 'event_date' ) ?></span>
                <span class="about__section__list__item__desc"><?php
                  echo $groups->display( 'description' ) ?></span>
              </li>
              <?php
            }
          }
          ?>
        </ul>
      </div>
    </div>
  </div>
</section>