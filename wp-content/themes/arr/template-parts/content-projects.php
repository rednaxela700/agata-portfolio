<section class="portfolio project">
  <div class="portfolio__header">
    <h2 class="portfolio__header__title">PROJECTS</h2>
  </div>
  <div class="project__container">
    <?php
    $project_posts        = get_posts( [
      'category' => 3
    ] );
    foreach ( $project_posts as $post ) :
      $post_thumbnail_id = get_post_meta( $post->ID )['_thumbnail_id'][0];
      $post_heading_color = get_post_meta( $post->ID )['heading_color'][0];
      if ( empty( $post_heading_color ) ) {
        $post_heading_color = '#fff';
      }
      ?>
      <figure class="project__item" style="position: relative;">
        <?php
        echo wp_get_attachment_image( $post_thumbnail_id, 'small' ); ?>
        <figcaption class="project__item__overlay">
          <a href="<?php
          echo $post->guid ?>" class="project__link">
            <h3 class="project__title" style="color: <?php
            echo $post_heading_color; ?>;"><?php
              the_title()
              ?></h3>
          </a>
        </figcaption>
      </figure>
    <?php
    endforeach;
    ?>
  </div>
</section>
